LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),phone1)

RADIO_FILES := $(wildcard $(LOCAL_PATH)/radio/*)
$(foreach f, $(notdir $(RADIO_FILES)), \
    $(call add-radio-file,radio/$(f)))

FIRMWARE_IMAGES := \
abl \
aop \
bluetooth \
cpucp \
devcfg \
dsp \
featenabler \
hyp \
imagefv \
keymaster \
modem \
multiimgoem \
qupfw \
shrm \
tz \
uefisecapp \
xbl \
xbl_config

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)

endif
