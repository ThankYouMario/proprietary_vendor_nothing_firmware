LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),phone2)

RADIO_FILES := $(wildcard $(LOCAL_PATH)/radio/*)
$(foreach f, $(notdir $(RADIO_FILES)), \
    $(call add-radio-file,radio/$(f)))

FIRMWARE_IMAGES := \
    abl \
    aop \
    aop_config \
    bluetooth \
    cpucp \
    devcfg \
    dsp \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    modem \
    multiimgoem \
    multiimgqti \
    qupfw \
    qweslicstore \
    shrm \
    tz \
    uefi \
    uefisecapp \
    xbl \
    xbl_config \
    xbl_ramdump

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)

endif
